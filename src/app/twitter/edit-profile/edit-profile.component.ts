import { Component, Input, OnChanges, SimpleChanges, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { ProfileService } from '../profile.service';
import { User, UserResponse } from '../profile/profile.component';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: [
    '../css/style.css',
    './edit-profile.component.css',
    '../css/all.css',
  ]
})
export class EditProfileComponent implements OnChanges {

  constructor(
    private profileService: ProfileService,
    private formBuilder: FormBuilder
  ) { }

  @Input() profile: User = {
    id: 0,
    first_name: "",
    last_name: "",
    username: "",
    email: "",
    createdAt: new Date(),
    updatedAt: new Date()
  };
  @Input() isVisible: boolean = false;
  @Output() profileUpdateEvent = new EventEmitter<User>();
  @Output() crossButtonClick = new EventEmitter<boolean>();

  editProfileForm = this.formBuilder.group({
    first_name: '',
    last_name: '',
    username: '',
    email: ''
  });

  onCrossButtonClick() {
    this.crossButtonClick.emit(true);
  }
  showForm() {
    this.isVisible = true;
  }
  ngOnChanges(changes: SimpleChanges) {
    if (changes['profile']) {
      this.editProfileForm.patchValue({
        first_name: this.profile.first_name,
        last_name: this.profile.last_name,
        username: this.profile.username,
        email: this.profile.email
      });
    }
    if (changes['isVisible']) {
      console.log("You just clicked edit profile button!");
      console.log(changes);
    }
  }
  updateProfile() {
    console.log("calling from updateProfile method");
    const formData = this.editProfileForm.value;
    this.profileService.updateProfile(formData).subscribe(
      (data: UserResponse) => {
        console.log("Updated User");
        console.log(data.user.first_name);
        this.profileUpdateEvent.emit(data.user);
      },
      (error) => {
        alert(error.error.error);
      }
    )
  }
}
