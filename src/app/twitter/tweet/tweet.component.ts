import { Component, Input, OnInit } from '@angular/core';
import {Like, Tweet, TweetWithCommentAndLike } from '../show-profile/show-profile.component';
import { GeneralService } from '../general.service';
import { LikeService } from '../like.service';
import { FormBuilder } from '@angular/forms';
import { CommentService } from '../comment.service';
import { User } from '../profile/profile.component';

@Component({
  selector: 'app-tweet',
  templateUrl: './tweet.component.html',
  styleUrls: [
    '../css/style.css',
    './tweet.component.css'
  ]
})
export class TweetComponent implements OnInit {

  constructor(
    private generalService: GeneralService,
    private likeService: LikeService,
    private commentService: CommentService,
    private formBuilder: FormBuilder
  ) {}
  heartFill:string = "";
  isCommentsVisible:boolean = false;
  @Input() tweet: TweetWithCommentAndLike = {
    id: 0,
    tweet: "",
    comment_id: 0,
    image_url: "",
    createdAt: new Date(),
    updatedAt: new Date(),
    User: {
      id: 0,
      first_name: "",
      last_name: "",
      email: "",
      username: "",
      createdAt: new Date(),
      updatedAt: new Date()
    },
    Likes: [],
    Comment: [],
  }
  commentForm = this.formBuilder.group({
    comment: ''
  });

  @Input() loggedInUser: User = {
    id: 0,
    first_name: "",
    last_name: "",
    email: "",
    username: "",
    createdAt: new Date(),
    updatedAt: new Date()
  };
  ngOnInit(): void {
    this.showFillOrEmptyHeartIcon();
  }
  fillHeartIcon() {
    this.heartFill = "-fill";
  }
  emptyHeartIcon() {
    this.heartFill = "";
  }
  toggleLike() {
    if (this.heartFill == "-fill") {
      this.unlike(this.tweet.id);
    }
    else if (this.heartFill == "") {
      this.like(this.tweet.id);
    }
  }
  toggleComment() {
    this.isCommentsVisible = !this.isCommentsVisible;
  }
  getHashedEmail(email: string) {
    return this.generalService.getHashedEmail(email);
  }
  like(tweetId: number) {
    this.likeService.like(tweetId).subscribe({
      next: (data:Like) => {
        alert("Like operation succeed");
        this.tweet.Likes.push(data);
        this.fillHeartIcon();
      },
      error: (error) => {
        alert(error.error.error);
      }
  });
  }
  unlike(tweetId: number) {
    this.likeService.unlike(tweetId).subscribe({
      next: (data:Like) => {
        alert("unlike operation succeed");
        this.emptyHeartIcon();
        const index = this.tweet.Likes.indexOf(data);
        this.tweet.Likes.splice(index);
      },
      error: (error) => {
        alert(error.error.error);
      }
  });
  }
  comment(){
    const formData = this.commentForm.value;
    this.commentService.comment(this.tweet.id,formData.comment).subscribe({
      next: (data:{tweet:Tweet}) => {
        this.commentForm.reset();
        console.log(data.tweet);
        //in order to add into comment array we need to append User in result
        data.tweet.User = this.loggedInUser;
        this.tweet.Comment.push(data.tweet);
      },
      error: (error) => {
        alert(error.error.error);
      }
  })
  }
  showFillOrEmptyHeartIcon(){
    this.tweet.Likes.forEach((Like) => {
      console.log(Like);
      console.log(this.loggedInUser);
      if(Like.user_id == this.loggedInUser.id){
        console.log("User already liked!");
        this.fillHeartIcon();
      }
    })
  }
}
