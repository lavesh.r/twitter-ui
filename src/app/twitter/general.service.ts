import { Injectable } from '@angular/core';
import * as CryptoJS from 'crypto-js';

@Injectable({
  providedIn: 'root'
})
export class GeneralService {

  constructor() { }
  getHashedEmail(email: string) {
    const normalizedEmail = email.trim().toLowerCase();
    const hash = CryptoJS.MD5(normalizedEmail).toString();
    return hash;
  }
}
