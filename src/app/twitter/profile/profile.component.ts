import { Component, OnInit, ChangeDetectorRef, Output, EventEmitter } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Observable, throwError } from 'rxjs';
import { ProfileService } from '../profile.service';
import { GeneralService } from '../general.service';
import { Router } from '@angular/router';
import { PaginatedTweetResponse, TweetWithCommentAndLike } from '../show-profile/show-profile.component';
import { TweetService } from '../tweet.service';


@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: [
    '../css/style.css',
    './profile.component.css',
    '../css/all.css'
  ]
})
export class ProfileComponent implements OnInit {
  constructor(
    private profileService: ProfileService,
    private router: Router,
    private generalService: GeneralService,
    private tweetService: TweetService

  ) { }


  profile: User = {
    id: 0,
    first_name: "",
    last_name: "",
    username: "",
    email: "",
    createdAt: new Date(),
    updatedAt: new Date()
  };


  followings: User[] = [];
  followers: User[] = [];
  isSearchOutputVisible = false;
  isEditProfileFormVisible = false;
  searchUsers: User[] = [];
  tweets: TweetWithCommentAndLike[] = [];
  followingLimit = 5;
  followingOffset = 0;
  followerLimit = 5;
  followerOffset = 0;
  tweetLimit = 5;
  tweetOffset = 0;
  followerPaginateIsVisible: boolean = false;
  followingPaginateIsVisible: boolean = false;
  tweetPaginateIsVisible: boolean = false;
  ngOnInit(): void {
    this.fetchProfile();
    this.fetchFollowings();
    this.fetchFollowers();
    this.fetchTweets();
  }
  fetchProfile() {
    this.profileService.getProfile().subscribe({
      next: (data: UserResponse) => {
        this.profile = data.user;
        console.log(this.profile);
        // console.log(this.profile[user].first_name);
      },
      error: (error) => {
        alert(error.error.error);
      }
    });
  }
  fetchFollowings() {
    this.profileService.getFollowings(this.followingLimit, this.followingOffset).subscribe({
      next: (data: PaginatedFollowingResponse) => {
        this.followingOffset += this.followingLimit;
        console.log(data.users);
        data.users.forEach((user) => {
          this.followings.push(user);
        });
        if (data.totalFollowingCount > this.followings.length) {
          this.followingPaginateIsVisible = true;
        }
        else {
          this.followingPaginateIsVisible = false;
        }
      },
      error: (error) => {
        alert(error.error.error);
      }
    });
  }
  fetchFollowers() {
    this.profileService.getFollowers(this.followerLimit, this.followerOffset).subscribe({
      next: (data: PaginatedFollowerResponse) => {
        this.followerOffset += this.followerLimit;
        console.log(data.users);
        data.users.forEach((user) => {
          this.followers.push(user);
        });
        if (data.totalFollowerCount > this.followers.length) {
          this.followerPaginateIsVisible = true;
        }
        else {
          this.followerPaginateIsVisible = false;
        }
      },
      error: (error) => {
        alert(error.error.error);
      }
    });
  }
  fetchTweets() {
    this.tweetService.getAuthUserTweetsWithLikesAndComments(this.tweetLimit, this.tweetOffset).subscribe({
      next: (data: PaginatedTweetResponse) => {
        this.tweetOffset += this.tweetLimit;
        data.tweets.forEach((tweet) => {
          this.tweets.push(tweet);
        });
        if (data.totalTweetCount > this.tweets.length) {
          this.tweetPaginateIsVisible = true;
        }
        else {
          this.tweetPaginateIsVisible = false;
        }
      },
      error: (error) => {
        alert(error.error.error);
      }
    });
  }

  navigateSearchUser(id: Number) {
    alert("navigate method called!");
    this.router.navigate(['/profile/', id]);
  }

  removeFollowing(following: User) {
    if (confirm(`Do You want to unfollow ${following.username} ?`)) {
      this.profileService.removeFollowing(following.id).subscribe({
        next: (data) => {
          console.log("Called from following ");
          console.log(following);
          let index = this.followings.indexOf(following, 0);
          this.followings.splice(index, 1);
          console.log(this.followings);
        },
        error: (error) => {
          alert(error.error.error);
        }
      });
    }
  }
  removeFollower(follower: User) {
    if (confirm(`Do You want to remove ${follower.username} from followers`)) {
      this.profileService.removeFollower(follower.id).subscribe({
        next: (data) => {
          console.log(data);
          let index = this.followers.indexOf(follower, 0);
          this.followers.splice(index);
        },
        error: (error) => {
          alert(error.error.error);
        }
      });
    }
  }
  disableSearchOutput() {
    this.isSearchOutputVisible = !this.isSearchOutputVisible;
  }
  disableEditProfileFormOutput() {
    this.isEditProfileFormVisible = false;
  }
  enableEditProfileFormOutput() {
    this.isEditProfileFormVisible = true;
  }
  search(text: string) {
    if (text.length > 0) {
      this.isSearchOutputVisible = true;
      this.profileService.userSearch(text).subscribe({
        next: (data: UserSearchResponse) => {
          this.searchUsers = data.users;
        },
        error:(error) => {
          alert(error.error.error);
        }
    });
    }
    else {
      this.isSearchOutputVisible = false;
    }
  }
  profileUpdate(data: User) {
    console.log("Profile update on parent side is called!");
    this.profile = data;
    this.disableEditProfileFormOutput();
  }
  getHashedEmail(email: string) {
    return this.generalService.getHashedEmail(email);
  }

}
export interface UserResponse {
  user: User
}
export interface User {
  id: number,
  first_name: string,
  last_name: string,
  username: string,
  email: string,
  createdAt: Date,
  updatedAt: Date
}
export interface FollowingResponse {
  users: User[];
}
export interface PaginatedFollowerResponse {
  users: User[];
  totalFollowerCount: number
}
export interface PaginatedFollowingResponse {
  users: User[];
  totalFollowingCount: number;
}
export interface FollowerResponse {
  users: User[];
}
export interface UserSearchResponse {
  users: User[];
}
