import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { SignoutService } from 'src/app/auth/signout.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: [
    '../css/style.css',
    './sidebar.component.css',
    '../css/all.css'
  ]
})
export class SidebarComponent {

  constructor(
    private signoutService: SignoutService,
    private router: Router
  ){}

  //for signout
  signout(){
    this.signoutService.signout().subscribe({
      next: (data) => {
        console.log(data);
        this.router.navigate(['/signin']);
      },
      error: (error) => {
        alert(error.error.error);
      }
  })
  }

}
