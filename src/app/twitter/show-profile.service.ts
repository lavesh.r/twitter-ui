import { Injectable } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { FollowerResponse, FollowingResponse, UserResponse } from './profile/profile.component';
import { PaginatedTweetResponse, TweetResponse } from './show-profile/show-profile.component';
import { environment } from 'src/environment';

@Injectable({
  providedIn: 'root'
})
export class ShowProfileService {

  constructor(
    private http: HttpClient
  ) { }

  getProfileFromId(id:number):Observable<UserResponse>{
    let url = environment.apiUrl + `/users/profile`;
    return this.http.get<UserResponse>(url,{
      withCredentials:true,
      params:{
        userId: id
      }
    });
  }

  getFollowersFromId(id:number):Observable<FollowerResponse>{
    let url = environment.apiUrl + `/users/followers`;
    return this.http.get<FollowerResponse>(url,{
      withCredentials:true,
      params: {
        userId: id
      }
    });
  }
  getFollowingsFromId(id:number):Observable<FollowingResponse>{
    let url = environment.apiUrl + `/users/followings`;
    return this.http.get<FollowingResponse>(url,{
      withCredentials:true,
      params: {
        userId: id
      }
    });
  }
  follow(id:number):Observable<{message:string}>{
    let url = environment.apiUrl + `/users/follow`;
    return this.http.put<{message:string}>(url,{userId:id},{withCredentials:true});
  }
  unfollow(id:number):Observable<{message:string}>{
    let url = environment.apiUrl + `/users/unfollow`;
    return this.http.put<{message:string}>(url,{userId:id},{withCredentials:true});
  }

  getTweetsWithLikesAndCommentsFromId(id:number,tweetLimit:number,tweetOffset:number):Observable<PaginatedTweetResponse>{
    let url = environment.apiUrl + `/tweets/users/with-likes-follows`;
    return this.http.get<PaginatedTweetResponse>(url,{
      withCredentials:true,
      params:{
        userId: id,
        limit: tweetLimit,
        offset: tweetOffset
      }
    });
  }
}
