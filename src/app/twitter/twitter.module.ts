import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfileComponent } from './profile/profile.component';
import { ReactiveFormsModule } from '@angular/forms';
import { EditProfileComponent } from './edit-profile/edit-profile.component';
import { AppRoutingModule } from '../app-routing.module';
import { SidebarComponent } from './sidebar/sidebar.component';
import { ShowProfileComponent } from './show-profile/show-profile.component';
import { GravatarModule, GravatarConfig, FALLBACK, RATING } from 'ngx-gravatar';
import { TweetComponent } from './tweet/tweet.component';
import { FeedComponent } from './feed/feed.component';

const gravatarConfig: GravatarConfig = {
  fallback: FALLBACK.identicon,
};


@NgModule({
  declarations: [
    ProfileComponent,
    EditProfileComponent,
    SidebarComponent,
    ShowProfileComponent,
    TweetComponent,
    FeedComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    AppRoutingModule,
    GravatarModule.forRoot(gravatarConfig)
  ],
  exports: [
    ProfileComponent
  ]
})
export class TwitterModule { }
