import { Component, OnInit, Input } from '@angular/core';
import { GeneralService } from '../general.service';
import { LikeService } from '../like.service';
import { CommentService } from '../comment.service';
import { FormBuilder } from '@angular/forms';
import { Like, Tweet, TweetWithCommentAndLike } from '../show-profile/show-profile.component';
import { User } from '../profile/profile.component';
import { ProfileService } from '../profile.service';
import { ShowProfileService } from '../show-profile.service';
import { TweetService } from '../tweet.service';

@Component({
  selector: 'app-feed',
  templateUrl: './feed.component.html',
  styleUrls: [
    '../css/style.css',
    './feed.component.css',
    '../css/all.css'
  ]
})
export class FeedComponent implements OnInit {

  constructor(
    private profileService: ProfileService,
    private tweetService: TweetService,
    private generalService: GeneralService,
    private formBuilder: FormBuilder
  ) { }
  tweets: TweetWithCommentAndLike[] = [];
  loggedInUser: User = {
    id: 0,
    first_name: "",
    last_name: "",
    username: "",
    email: "",
    createdAt: new Date(),
    updatedAt: new Date()
  };
  tweetPostForm = this.formBuilder.group({
    tweet: ''
  });
  tweetOffset: number = 0;
  tweetLimit: number = 5;
  paginateIsVisible: boolean = false;
  cursorLoading: string = "loading";
  ngOnInit(): void {
    this.cursorLoading = "loading";
    this.fetchLoggedInUser();
    this.fetchAllFollowingsTweet();
    this.cursorLoading = "";
  }
  fetchLoggedInUser() {
    this.profileService.getProfile().subscribe({
      next: (data) => {
        this.loggedInUser = data.user;
      },
      error: (error) => {
        alert(error.error.error);
      }
    });
  }
  postTweet() {
    const tweet = this.tweetPostForm.value['tweet'];
    this.tweetService.postTweet(tweet).subscribe({
      next: (data) => {
        this.tweetPostForm.reset();
        alert("Tweet posted successfully!");
      },
      error: (error) => {
        alert(error.error.error);
      }
    });
  }
  fetchAllFollowingsTweet() {
    this.cursorLoading = "loading";
    this.tweetService.getTweetsWithLikesAndComments(this.tweetLimit, this.tweetOffset).subscribe({
      next: (data) => {
        this.tweetOffset += this.tweetLimit;
        console.log(this.tweets);
        console.log(data.tweets);
        data.tweets.forEach((tweet) => {
          this.tweets.push(tweet);
        });
        if (data.totalTweetCount > this.tweets.length) {
          this.paginateIsVisible = true;
        }
        else {
          this.paginateIsVisible = false;
        }
        // this.cursorLoading = "";
      },
      error: (error) => {
        alert(error.error.error);
      }
    })

  }
  getHashedEmail(email: string) {
    return this.generalService.getHashedEmail(email);
  }
}
