import { Injectable } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Like } from './show-profile/show-profile.component';
import { environment } from 'src/environment';

@Injectable({
  providedIn: 'root'
})
export class LikeService {

  constructor(
    private http: HttpClient
  ) { }

  like(tweetId: number):Observable<Like> {
    let url = environment.apiUrl + `/tweets/like`;
    //passing an empty object for post body
    // this.http.put<Like>(url,)
    return this.http.put<Like>(url, {tweetId: tweetId}, {
      withCredentials: true
    });

  }
  unlike(tweetId: number):Observable<Like> {
    let url = environment.apiUrl + `/tweets/unlike`;
    //passing an empty object for post body
    return this.http.put<Like>(url, {tweetId: tweetId}, {
      withCredentials: true
    });
  }
}
