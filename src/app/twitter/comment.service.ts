import { Injectable } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Tweet, TweetResponse } from './show-profile/show-profile.component';
import { environment } from 'src/environment';

@Injectable({
  providedIn: 'root'
})
export class CommentService {

  constructor(
    private http: HttpClient
  ) { }
  comment(tweetId:number,comment:string | null | undefined):Observable<{tweet: Tweet}>{
    let url = environment.apiUrl+`/tweets/comment`;
    return this.http.put<{tweet:Tweet}>(url,{commentId:tweetId,tweet:comment},{withCredentials:true});
  }
}
