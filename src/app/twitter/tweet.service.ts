import { Injectable } from '@angular/core';
import { PaginatedTweetResponse, TweetResponse } from './show-profile/show-profile.component';
import { HttpClient } from '@angular/common/http';
import { User } from './profile/profile.component';
import { environment } from 'src/environment';
import { Observable } from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class TweetService {

  constructor(
    private http: HttpClient
  ) { }

  getTweetsWithLikesAndComments(tweetLimit:number,tweetOffset:number):Observable<PaginatedTweetResponse>{
    let url = environment.apiUrl + `/tweets/followings`;
    return this.http.get<PaginatedTweetResponse>(url,{
      withCredentials:true,
      params: {
        limit: tweetLimit,
        offset: tweetOffset
      }
    });
  }

  postTweet(tweet:string | null | undefined):Observable<TweetResponse>{
    let url = environment.apiUrl + `/tweets`;
    return this.http.put<TweetResponse>(url,{tweet:tweet},{withCredentials:true});
  }
  getAuthUserTweetsWithLikesAndComments(tweetLimit:number,tweetOffset:number):Observable<PaginatedTweetResponse>{
    let url = environment.apiUrl + `/tweets`;
    return this.http.get<PaginatedTweetResponse>(url,{
      withCredentials:true,
      params: {
        limit: tweetLimit,
        offset: tweetOffset
      }
    });
  }
}
