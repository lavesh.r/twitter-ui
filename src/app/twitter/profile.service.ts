import { Injectable } from '@angular/core';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { FollowerResponse, FollowingResponse, PaginatedFollowerResponse, PaginatedFollowingResponse, User, UserResponse, UserSearchResponse } from './profile/profile.component';
import { environment } from 'src/environment';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  constructor(
    private http: HttpClient
  ) { }

  getProfile():Observable<UserResponse> {
    let url = environment.apiUrl + `/users/auth/profile`;
    return this.http.get<UserResponse>(url, { withCredentials: true });
  }
  getFollowings(followingLimit:number,followingOffset:number):Observable<PaginatedFollowingResponse> {
    let url = environment.apiUrl +`/users/auth/followings`;
    return this.http.get<PaginatedFollowingResponse>(url, {
      withCredentials: true,
      params: {
        limit: followingLimit,
        offset: followingOffset
      }
    });
  }
  getFollowers(followerLimit:number,followerOffset:number):Observable<PaginatedFollowerResponse> {
    let url = environment.apiUrl +`/users/auth/followers`;
    return this.http.get<PaginatedFollowerResponse>(url, {
      withCredentials: true,
      params: {
        limit: followerLimit,
        offset: followerOffset
      }
    });
  }
  removeFollowing(id: number):Observable<{message: string}>{
    let url = environment.apiUrl + `/users/unfollow`;
    return this.http.put<{message: string}>(url, { userId: id }, { withCredentials: true });
  }
  removeFollower(id: number):Observable<{message: string}> {
    let url = environment.apiUrl + `/users/remove-follower`;
    return this.http.put<{message: string}>(url, { userId: id }, { withCredentials: true });
  }

  userSearch(text: string):Observable<UserSearchResponse> {
    let url = environment.apiUrl + `/users/search`;
    return this.http.get<UserSearchResponse>(url, {
      withCredentials: true,
      params:{
        searchKeyword: text
      }
    });
  }

  updateProfile(data: Partial<{
    first_name: string | null,
    last_name: string | null,
    username: string | null,
    email: string | null
  }>):Observable<UserResponse> {
    console.log("calling from service");
    console.log(data);
    let url = environment.apiUrl + `/users`;
    return this.http.patch<UserResponse>(url, {
      first_name: data.first_name,
      last_name: data.last_name,
      username: data.username,
      email: data.email
    }, { withCredentials: true });
  }


}
