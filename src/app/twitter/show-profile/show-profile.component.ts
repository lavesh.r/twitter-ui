import { Component, OnInit, Input } from '@angular/core';
import { FollowerResponse, FollowingResponse, User, UserResponse } from '../profile/profile.component';
import { ActivatedRoute } from '@angular/router';
import { ShowProfileService } from '../show-profile.service';
import { ProfileService } from '../profile.service';
import { LikeService } from '../like.service';
import { GeneralService } from '../general.service';

@Component({
  selector: 'app-show-profile',
  templateUrl: './show-profile.component.html',
  styleUrls: [
    '../css/style.css',
    './show-profile.component.css',
    '../css/all.css'
  ]
})
export class ShowProfileComponent implements OnInit {

  constructor(private route: ActivatedRoute,
    private showProfileService: ShowProfileService,
    private profileService: ProfileService,
    private likeService: LikeService,
    private generalService: GeneralService
  ) { }

  parentUser: User = {
    id: 0,
    first_name: "",
    last_name: "",
    username: "",
    email: "",
    createdAt: new Date(),
    updatedAt: new Date()
  };
  profile: User = {
    id: 0,
    first_name: "",
    last_name: "",
    username: "",
    email: "",
    createdAt: new Date(),
    updatedAt: new Date()
  };


  showType = "follow";
  followings: User[] = [];
  followers: User[] = [];
  tweets: TweetWithCommentAndLike[] = [];
  tweetLimit: number = 2;
  tweetOffset: number = 0;
  tweetPaginateIsVisible: boolean = false;
  cursorLoading: string = "loading";


  fetchId(): number {
    return Number(this.route.snapshot.paramMap.get("userId"));
  }
  fetchParentUser() {
    this.profileService.getProfile().subscribe({
      next: (data) => {
        this.parentUser = data.user;
      },
      error: (error) => {
        alert(error.error.error);
      }
    });
  }
  fetchProfile(id: number) {
    this.showProfileService.getProfileFromId(id).subscribe({
      next: (data) => {
        this.profile = data.user;
      },
      error: (error) => {
        alert(error.error.error);
      }
    });

  }
  fetchFollowers(id: number) {
    this.showProfileService.getFollowersFromId(id).subscribe({
      next: (data) => {
        this.followers = data.users;
        this.showFollowOrUnfollowButton();
      },
      error: (error) => {
        alert(error.error.error);
      }
    });

  }
  fetchFollowings(id: number) {
    this.showProfileService.getFollowingsFromId(id).subscribe({
      next: (data) => {
        this.followings = data.users;
        this.showFollowOrUnfollowButton();
      },
      error: (error) => {
        alert(error.error.error);
      }
    })
  }
  showFollowOrUnfollowButton() {
    console.log("called from show-profile");
    console.log(this.followers);
    this.followers.map((follower) => {
      if (follower.id == this.parentUser.id) {
        this.showType = "following";
      }
    });
  }
  toggleShowType() {
    if (this.showType == "following") {
      this.showType = "follow";
    }
    else if (this.showType == "follow") {
      this.showType = "following";
    }
  }
  follow(id: number) {
    this.showProfileService.follow(id).subscribe({
      next: (data) => {
        this.toggleShowType();
        this.followers.push(this.parentUser);
      },
      error: (error) => {
        alert(error.error.error);
      }
    })
  }
  unfollow(id: number) {
    this.showProfileService.unfollow(id).subscribe({
      next: (data) => {
        this.toggleShowType();
        let userIndex = this.followers.indexOf(this.parentUser);
        this.followers.splice(userIndex);
      },
      error: (error) => {
        alert(error.error.error);
      }
    })
  }
  getTweetsWithFollowCountAndLikeCount(id: number) {
    this.showProfileService.getTweetsWithLikesAndCommentsFromId(id, this.tweetLimit, this.tweetOffset).subscribe({
      next: (data: PaginatedTweetResponse) => {
        // this.tweets = data.tweets;
        console.log(data);
        this.tweetOffset += this.tweetLimit;
        data.tweets.forEach((tweet) => {
          this.tweets.push(tweet);
        });
        if (data.totalTweetCount > this.tweets.length) {
          this.tweetPaginateIsVisible = true;
        }
        else {
          this.tweetPaginateIsVisible = false;
        }
      },
      error: (error) => {
        console.log(error);
      }
    })
  }
  getHashedEmail(email: string) {
    return this.generalService.getHashedEmail(email);
  }
  ngOnInit(): void {
    let id = this.fetchId();
    //fetching parent user or loggedin user
    this.fetchParentUser();
    this.fetchProfile(id);
    this.fetchFollowers(id);
    this.fetchFollowings(id);
    this.getTweetsWithFollowCountAndLikeCount(id);
  }
}
export interface Like {
  id: number,
  user_id: number,
  tweet_id: number,
  createdAt: number,
  updatedAt: number
}
export interface Tweet {
  id: number,
  tweet: string,
  comment_id: number,
  image_url: string,
  createdAt: Date,
  updatedAt: Date,
  User: User
}
export interface Comment {
  id: number,
  tweet: string,
  comment_id: number,
  image_url: string,
  createdAt: Date,
  updatedAt: Date,
  User: User

}
export interface TweetWithCommentAndLike {
  id: number,
  tweet: string,
  comment_id: number,
  image_url: string,
  createdAt: Date,
  updatedAt: Date,
  User: User
  Comment: Tweet[],
  Likes: Like[]
}
export interface TweetResponse {
  tweets: TweetWithCommentAndLike[]
}
export interface PaginatedTweetResponse {
  tweets: TweetWithCommentAndLike[],
  totalTweetCount: number
}
