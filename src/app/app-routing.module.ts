import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SigninComponent } from './auth/signin/signin.component';
import { SignupComponent } from './auth/signup/signup.component';
import { ProfileComponent } from './twitter/profile/profile.component';
import { AuthGuard } from './auth/auth.guard';
import { ShowProfileComponent } from './twitter/show-profile/show-profile.component';
import { FeedComponent } from './twitter/feed/feed.component';
import { RouteGuard } from './auth/route.guard';

const routes: Routes = [
  { path: 'signin', component: SigninComponent,canActivate: [RouteGuard]},
  { path: 'signup', component: SignupComponent,canActivate: [RouteGuard]},
  { path: 'profile', component: ProfileComponent, canActivate: [AuthGuard] },
  { path: 'profile/:userId', component: ShowProfileComponent, canActivate: [AuthGuard] },
  { path: 'home', component: FeedComponent, canActivate: [AuthGuard] },
  { path: '**', redirectTo: 'home' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
