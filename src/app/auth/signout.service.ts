import { Injectable } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { environment } from 'src/environment';

@Injectable({
  providedIn: 'root'
})
export class SignoutService {
  constructor(
    private http: HttpClient
  ) { }
  hasErrors = false;
  errors = [];
  signout(){
    //as signin and signup returns same response thats why using signinResponse here
    return this.http.post(environment.apiUrl + "/auth/signout",{},{withCredentials:true});
  }
}
