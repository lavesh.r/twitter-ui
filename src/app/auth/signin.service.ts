import { Injectable } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { SigninResponse } from './signin/signin.component';
import { environment } from 'src/environment';

@Injectable({
  providedIn: 'root'
})
export class SigninService {

  constructor(
    private http: HttpClient
  ) { }
  hasErrors = false;
  errors = [];
  signin(formData: FormGroup): Observable<SigninResponse>{
    return this.http.post<SigninResponse>(environment.apiUrl + "/auth/signin",formData.value,{withCredentials:true});
  }
}
