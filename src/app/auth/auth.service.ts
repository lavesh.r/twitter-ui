import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private http: HttpClient
  ) { }
  hasErrors = false;
  errors = [];
  isLoggedIn(): Promise<boolean> {
    return new Promise<boolean>((resolve, reject) => {
      this.http.get<{ status: string }>(environment.apiUrl + "/users/validate", { withCredentials: true })
        .subscribe({
          next:(data) => {
            if (data.status == "verified") {
              resolve(true);
            }
            else {
              resolve(false);
            }
          },
          error: (error) => {
            resolve(false);
          }
    })
    });
  }

}
