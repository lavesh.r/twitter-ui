import { Component } from '@angular/core';
import { SignupService } from '../signup.service';
import { FormGroup, FormControl } from '@angular/forms';
import { SigninResponse } from '../signin/signin.component';
import { Router } from '@angular/router';


@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: [
    '../css/style.css',
    '../css/fonts/linearicons/style.css',
    './signup.component.css'
]
})
export class SignupComponent {
  constructor(
    private signupService: SignupService,
    private router: Router
  ){}

  signupForm = new FormGroup({
    first_name: new FormControl(''),
    last_name: new FormControl(''),
    email: new FormControl(''),
    username: new FormControl(''),
    password: new FormControl(''),
    confirm_password: new FormControl(''),
  });

  hasErrors = false;
  errorMessage = "";
  onSubmit() {
    this.signupService.signup(this.signupForm)
      .subscribe({next:(data: SigninResponse) => {
        this.hasErrors = false;
          console.log(data);
          this.router.navigate(['/profile']);
      },error:(errorResponse) => {
        this.hasErrors = true;
        this.errorMessage = errorResponse.error.error;
      }})
  }
}

