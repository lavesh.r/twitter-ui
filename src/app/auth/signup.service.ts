import { Injectable } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { SigninResponse } from './signin/signin.component';
import { environment } from 'src/environment';

@Injectable({
  providedIn: 'root'
})
export class SignupService {

  constructor(
    private http: HttpClient
  ) { }
  hasErrors = false;
  errors = [];
  signup(formData: FormGroup){
    //as signin and signup returns same response thats why using signinResponse here
    return this.http.put<SigninResponse>(environment.apiUrl + "/auth/signup",formData.value);
  }
}
