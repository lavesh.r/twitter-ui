import { Component } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { SigninService } from '../signin.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: [
    '../css/style.css',
    '../css/fonts/linearicons/style.css',
    './signin.component.css'
  ]
})
export class SigninComponent {

  constructor(
    private signinService: SigninService,
    private router: Router
  ) { }
  signinForm = new FormGroup({
    email: new FormControl(''),
    password: new FormControl('')
  });

  hasErrors:boolean = false;
  errorMessage:string  = "";
  cursorLoading:string = "";
  onSubmit() {
    this.cursorLoading = "loading";
    this.signinService.signin(this.signinForm)
      .subscribe({next: (data: SigninResponse) => {
        this.cursorLoading = "";
        this.hasErrors = false;
          console.log(data);
          this.router.navigate(['/profile']);

      },error: (errorResponse) => {
        this.hasErrors = true;
        this.errorMessage = errorResponse.error.error;
      }})
  }
}

export interface SigninResponse{
  id: Number,
  token: string
}
